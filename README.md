# Apache Hadoop Base Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-hadoop-base/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-hadoop-base/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-hadoop-base/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-hadoop-base/commits/master)

This is just a base image for building another docker images!

The image is based on [adoptopenjdk/openjdk8](https://hub.docker.com/r/adoptopenjdk/openjdk8)

*	[adoptopenjdk/openjdk8:alpine-slim](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/alpine/Dockerfile.hotspot.releases.slim)
*	[adoptopenjdk/openjdk8:slim](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/ubuntu/Dockerfile.hotspot.releases.slim)

Native libraries `${HADOOP_HOME}/lib/native/*.so` work by utilising native glibc or glibc-compat from the base image.
Can be verified by running: `/opt/hadoop/bin/hadoop checknative -a`

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-hadoop-base:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-hadoop-base" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run

~~~sh
docker run -ti "registry.gitlab.com/rychly-edu/docker/docker-hadoop-base:latest" /bin/sh --login
~~~

## Configuration

Hadoop configuration can be reset by environment variables with prefixes `PROP_CORE`, `PROP_HDFS`, `PROP_YARN`, `PROP_MAPRED`, `PROP_HTTPFS`, `PROP_KMS`.
Names of the environment variables are transformed into names of the properties by replacements `___ => -`, `__ => _`, and `_ => .`.

For example, to configure a name-node and its data-nodes
(also with a multi-homed configuration for HDFS, YARN, and MapReduce, so the cluster will be reachable from multiple networks),
the following environment variables can be set in `docker-compose.yml` for both of nodes:

~~~yaml
   environment:
      - PROP_CORE_fs_defaultFS=hdfs://namenode:8020
      - PROP_CORE_hadoop_http_staticuser_user=root
      - PROP_CORE_hadoop_proxyuser_hue_hosts=*
      - PROP_CORE_hadoop_proxyuser_hue_groups=*
      - PROP_HDFS_dfs_namenode_name_dir=file:///hadoop/dfs/name
      - PROP_HDFS_dfs_datanode_data_dir=file:///hadoop/dfs/data
      - PROP_HDFS_dfs_webhdfs_enabled=true
      - PROP_HDFS_dfs_permissions_enabled=false
      - PROP_HDFS_dfs_namenode_rpc___bind___host=0.0.0.0
      - PROP_HDFS_dfs_namenode_servicerpc___bind___host=0.0.0.0
      - PROP_HDFS_dfs_namenode_http___bind___host=0.0.0.0
      - PROP_HDFS_dfs_namenode_https___bind___host=0.0.0.0
      - PROP_HDFS_dfs_client_use_datanode_hostname=true
      - PROP_HDFS_dfs_datanode_use_datanode_hostname=true
      - PROP_YARN_yarn_resourcemanager_bind___host=0.0.0.0
      - PROP_YARN_yarn_nodemanager_bind___host=0.0.0.0
      - PROP_YARN_yarn_nodemanager_bind___host=0.0.0.0
      - PROP_YARN_yarn_timeline___service_bind___host=0.0.0.0
      - PROP_MAPRED_yarn_nodemanager_bind___host=0.0.0.0
~~~

And then, a Docker entrypoint script needs to execute `/hadoop-set-props.sh', format the name-node, and start the corresponding servers.

~~~sh
# for the name-node
mkdir -p /hadoop/dfs/name
chown -R hadoop:hadoop /hadoop/dfs/name
hdfs namenode -format ${CLUSTER_NAME}
hdfs namenode
# for the data-node
mkdir -p /hadoop/dfs/data
chown -R hadoop:hadoop /hadoop/dfs/data
hdfs datanode
~~~
