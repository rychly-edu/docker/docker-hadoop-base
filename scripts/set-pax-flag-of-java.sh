#!/bin/sh

# fix ISSUE: os::commit_memory failed; error=Operation not permitted
# (AUFS does not support xattr, so we need to set the flag once again after execution of the container in its entrypoint)
# https://en.wikibooks.org/wiki/Grsecurity/Application-specific_Settings#Java
which setfattr && setfattr -n user.pax.flags -v em ${JAVA_HOME}/bin/java ${JAVA_HOME}/jre/bin/java
